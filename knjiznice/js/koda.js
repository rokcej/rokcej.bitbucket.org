
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

$(document).ready(function(){
	$("#izberiUporabnikaZaVnos").change(function(){
		$("#preberiEHRid").val($(this).val());
	});
	
	$("#izberiUporabnikaZaPrikaz").change(function(){
		$("#prikaziEHRid").val($(this).val())
	});

});
 
function novUporabnik() {
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datum = $("#kreirajDatumRojstva").val();
	
	var option = document.createElement(option);
	var izbiraVnos = $("#izberiUporabnikaZaVnos");
	var izbiraPrikaz = $("#izberiUporabnikaZaPrikaz");
	
	if(!ime || !priimek || !datum || ime.trim().length == 0 || priimek.trim().length == 0 || datum.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim izpolnite vsa polja!</span>");
    } else {
		kreirajEHR(ime, priimek, datum, 1, function(ehrId){
			izbiraVnos.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
	        izbiraPrikaz.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
	        $("#preberiEHRid").val(ehrId);
		});
    }
}


var generirano = false;
function generiraj() {
    if (!generirano) {
        generirano = true;
        generirajPodatke(1);
        generirajPodatke(2);
        generirajPodatke(3);
    }
}
function generirajPodatke(stPacienta) {
  var izbiraVnos = $("#izberiUporabnikaZaVnos");
  var izbiraPrikaz = $("#izberiUporabnikaZaPrikaz");
  var ime = "";
  var priimek = "";
  var kraj = "";
  var datumRojstva = "";
  
  switch(stPacienta) {
      case 1: 
          ime = "Jože";
          priimek = "Kranjc";
          datumRojstva = "1969-05-3T15:15";
          
          kreirajEHR(ime, priimek, datumRojstva, 0, function(ehrId){
          	alert("Generiran uporabnik št. : "+stPacienta+", ehrId: "+ehrId);
          	izbiraVnos.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
        	izbiraPrikaz.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
        	dodajVnoseGen(ehrId, "2017-05-01T12:00", 52, 114);
        	dodajVnoseGen(ehrId, "2017-05-02T12:00", 75, 115);
        	dodajVnoseGen(ehrId, "2017-05-03T12:00", 29, 105);
        	dodajVnoseGen(ehrId, "2017-05-04T12:00", 100, 103);
        	dodajVnoseGen(ehrId, "2017-05-05T12:00", 91, 102);
        	dodajVnoseGen(ehrId, "2017-05-06T12:00", 84, 110);
        	dodajVnoseGen(ehrId, "2017-05-07T12:00", 85, 112);
        	dodajVnoseGen(ehrId, "2017-05-08T12:00", 86, 113);
        	dodajVnoseGen(ehrId, "2017-05-09T12:00", 87, 114);
        	dodajVnoseGen(ehrId, "2017-05-10T12:00", 80, 115);
          });
          
          break;
      case 2:
          ime = "Primož";
          priimek = "Peterka";
          datumRojstva = "1972-10-11T20:11"
          
          kreirajEHR(ime, priimek, datumRojstva, 0, function(ehrId){
          	alert("Generiran uporabnik št. : "+stPacienta+", ehrId: "+ehrId);
          	izbiraVnos.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
        	izbiraPrikaz.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
        	dodajVnoseGen(ehrId, "2017-04-01T12:00", 93, 115);
        	dodajVnoseGen(ehrId, "2017-04-02T12:00", 100, 80);
        	dodajVnoseGen(ehrId, "2017-04-03T12:00", 99, 81);
        	dodajVnoseGen(ehrId, "2017-04-04T12:00", 88, 73);
        	dodajVnoseGen(ehrId, "2017-04-05T12:00", 35, 89);
        	dodajVnoseGen(ehrId, "2017-04-06T12:00", 41, 87);
        	dodajVnoseGen(ehrId, "2017-04-07T12:00", 77, 78);
        	dodajVnoseGen(ehrId, "2017-04-08T12:00", 66, 85);
        	dodajVnoseGen(ehrId, "2017-04-09T12:00", 25, 90);
        	dodajVnoseGen(ehrId, "2017-05-10T12:00", 25, 73);
          });
          
          break;
      case 3:
          ime = "Jaka";
          priimek = "Mladi";
          datumRojstva = "1999-12-25T01:23"
          
          kreirajEHR(ime, priimek, datumRojstva, 0, function(ehrId){
          	alert("Generiran uporabnik št. : "+stPacienta+", ehrId: "+ehrId);
          	izbiraVnos.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
        	izbiraPrikaz.append($("<option></option>").attr("value", ehrId).text(ime+" "+priimek));
        	dodajVnoseGen(ehrId, "2017-03-01T12:00", 100, 125);
        	dodajVnoseGen(ehrId, "2017-03-02T12:00", 85, 130);
        	dodajVnoseGen(ehrId, "2017-03-03T12:00", 60, 81);
        	dodajVnoseGen(ehrId, "2017-03-04T12:00", 97, 150);
        	dodajVnoseGen(ehrId, "2017-03-05T12:00", 78, 119);
        	dodajVnoseGen(ehrId, "2017-03-06T12:00", 81, 155);
        	dodajVnoseGen(ehrId, "2017-03-07T12:00", 83, 145);
        	dodajVnoseGen(ehrId, "2017-03-08T12:00", 94, 163);
        	dodajVnoseGen(ehrId, "2017-03-09T12:00", 71, 115);
        	dodajVnoseGen(ehrId, "2017-03-10T12:00", 89, 144);
          });
          
          break;
       default: break;
  }
}

function kreirajEHR(ime, priimek, datumRojstva, izpis, cback) {
    var sessionId = getSessionId();
    var ehrId = 0;
    var napaka = 1;
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
	    type: 'POST',
	    success: function (data) {
	        ehrId = data.ehrId;
	        var partyData = {
	            firstNames: ime,
	            lastNames: priimek,
	            dateOfBirth: datumRojstva,
	            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
	        };
	        
	        $.ajax({
	            url: baseUrl + "/demographics/party",
	            type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                if (party.action == 'CREATE' && izpis == 1) {
	                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR profil.</span>");
	                }
	            },
	            error: function(err) {
	            	if(izpis == 1)
	            		$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Napaka pri kreiranju</span>");
	            }
	        });
	        cback(ehrId);
	    }
	});
}

function dodajVnoseGen(ehrId, datum, diastolicni, sistolicni) {
	var sessionId = getSessionId();
	var datumInUra = datum;
	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	var podatki = {
		"ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": datumInUra,
	    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
	    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
	};
	var parametriZahteve = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: "Zdravnik"
	};
	$.ajax({
	    url: baseUrl + "/composition?" + $.param(parametriZahteve),
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(podatki),
	    success: function (res) {
	    },
	    error: function(err) {
	    	
	    }
	});
}
function dodajVnose() {
	var sessionId = getSessionId();
	
	var ehrId = $("#preberiEHRid").val();
	var datum = $("#datumVnosa").val();
	var diastolicni = $("#diastolicniPritisk").val();
	var sistolicni = $("#sistolicniPritisk").val();
	if (!ehrId || ehrId.trim().length == 0 || !datum || !diastolicni || !sistolicni || datum.trim().length == 0 || diastolicni.trim().length == 0 || sistolicni.trim().length == 0) {
		$("#vnesiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim izpolnite vsa polja!</span>");
	} else {
    	$.ajaxSetup({
    	    headers: {"Ehr-Session": sessionId}
    	});
    	var podatki = {
    		"ctx/language": "en",
    	    "ctx/territory": "SI",
    	    "ctx/time": datum,
    	    "vital_signs/blood_pressure/any_event/systolic": sistolicni,
    	    "vital_signs/blood_pressure/any_event/diastolic": diastolicni,
    	};
    	var parametriZahteve = {
    	    ehrId: ehrId,
    	    templateId: 'Vital Signs',
    	    format: 'FLAT',
    	    committer: "Zdravnik"
    	};
    	$.ajax({
    	    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    	    type: 'POST',
    	    contentType: 'application/json',
    	    data: JSON.stringify(podatki),
    	    success: function (res) {
    	    	$("#vnesiSporocilo").html(
              "<span class='obvestilo label label-success fade-in'> Uspešno vneseno." + "</span>");
    	    },
    	    error: function(err) {
    	    	$("#vnesiSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka'" + "'!</span>");
    	    }
    	});
    	
    	odzivNaVnos(diastolicni, sistolicni);
	}
}
function odzivNaVnos(dis, sis) {
    var maps = true;
    $("#rezultatMeritveVitalnihZnakov").html("");
    if (sis < 40 || dis < 15) {
        maps = false;
        $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-primary'><b>Meritve so neskladne! Prosim vnesite pravilne meritve.</b></span>");
	} else if (sis < 90 && dis < 80) {
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-info'><b>Vaš tlak je prenizek. V primeru slabega počutja se posvetujte z zdravnikom.</b></span>");
	    maps = true;
	} else if (sis <= 120 && dis < 90) {
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-success'><b>Vaš tlak je idealen, kar tako naprej.</b></span>");
	} else if (sis < 140 && dis < 100) {
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-success'><b>Vaš tlak je nekoliko povišan, vendar ne rabite skrbeti.</b></span>");
	} else if (sis < 160 && dis < 110) {
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-warning'><b>Vaš tlak je nadpovprečno visok, priporočamo obisk lekarne, kjer naj vam prodajalec svetuje glede zdravila.</b></span>");
	    maps = true;
	} else if (sis < 180 && dis < 120) {
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-danger'><b>Vaš tlak je previsok, čim prej obiščite zdravnika.</b></span>");
	    maps = true;
	} else if (sis >= 180 && dis >= 90) {
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-danger'><b>Vaš tlak je nevarno visok, takoj pojdite v bolnišnico ali k zdravniku.</b></span>");
	    maps = true;
	} else {
	    maps = false;
	    $("#rezultatMeritveVitalnihZnakov").html("<span class='text-center bg-primary'><b>Meritve so neskladne! Prosim vnesite pravilne meritve.</b></span>");
	}
	
	if (maps) {
	    if(navigator.geolocation) { //ce hoce okol domacega, potem ne rabim navigator
       		navigator.geolocation.getCurrentPosition(function(pos){
				var longitude = pos.coords.longitude;
				var latitude = pos.coords.latitude;
				$("#rezultatMeritveVitalnihZnakov").append("<p>Spodaj je prikazan zemljevid z vašo lokacijo in bližnjimi zdravstvenimi ustanovami.</p>")
				$("#rezultatMeritveVitalnihZnakov").append('<iframe class="mapce" width="450" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?key=AIzaSyAL0NItwh2Q5XQLan9FXCKBJZUsBOloxiE&q=lekarna&center='+latitude+','+longitude+'&zoom=10" allowfullscreen>" </iframe>');
				
	    	});
		} else {
			alert("Napaka pri prikazovanju zemljevida.");
		}
	}
}

function prikaziPodatke() {
	var sessionId = getSessionId();
	var ehrId = $("#prikaziEHRid").val();
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#prikaziSporocilo").html("<span class='obvestilo label label-warning fade-in'>Vnesite ehrId!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
					var diastolic = [];
					var systolic = [];
					var dates = [];
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	var dolzina = res.length;
					    	if (res.length > 0) {
						        for (var i in res) {
						            diastolic[dolzina - 1 - i] = res[i].diastolic;
						            systolic[dolzina - 1 - i] = res[i].systolic;
						            dates[dolzina - 1 - i] = res[i].time;
						        }
						        prikaziMeritve(diastolic, systolic, dates);
					    	} else {
					    		$("#prikaziSporocilo").html("<span class='obvestilo label label-warning fade-in'>Napaka pri branju podatkov!");
					    	}
					    	
					    },
					    error: function() {
					    	
					    }
					});
				
	    	},
	    	error: function(err) {
	    		$("#prikaziSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka!");
	    	}
		});
	}
}
function prikaziMeritve(dis, sis, dates) {
    var barva = "";
    
    var html = "<p><b>Seznam preteklih meritev uporabnika</b></p><ul>";
    for (var i = 0; i < dis.length; i++) {
        if (sis[i] < 40 || dis[i] < 15) {
            barva = "grey";
    	} else if (sis[i] < 90 && dis[i] < 80) {
    	    barva = "orange";
    	} else if (sis[i] <= 120 && dis[i] < 90) {
    	    barva = "green";
    	} else if (sis[i] < 140 && dis[i] < 100) {
    	    barva = "green";
    	} else if (sis[i] < 160 && dis[i] < 110) {
    	    barva = "orange";
    	} else if (sis[i] < 180 && dis[i] < 120) {
    	    barva = "OrangeRed";
    	} else if (sis[i] >= 180 && dis[i] >= 90) {
    	    barva = "red";
    	} else {
    	    barva = "grey";
    	}
        html += "<li style=\"color:"+barva+";\">Diastolični tlak: " + dis[i] + "mm Hg, Sistolični tlak: " + sis[i] + "mm Hg<br>Čas: "+ dates[i] +"</li>";
    }
    html += "</ul>"
    $("#izpisMeritev").html(html);
}
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
